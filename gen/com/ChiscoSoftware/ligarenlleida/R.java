/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.ChiscoSoftware.ligarenlleida;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int balafia=0x7f020000;
        public static final int basses=0x7f020001;
        public static final int bordetta=0x7f020002;
        public static final int butsenit=0x7f020003;
        public static final int camp=0x7f020004;
        public static final int cappont=0x7f020005;
        public static final int cappont_poli=0x7f020006;
        public static final int centre=0x7f020007;
        public static final int ic_launcher=0x7f020008;
        public static final int icon=0x7f020009;
        public static final int icon_medium=0x7f02000a;
        public static final int instituts=0x7f02000b;
        public static final int joc=0x7f02000c;
        public static final int llivia=0x7f02000d;
        public static final int magraners=0x7f02000e;
        public static final int mariola=0x7f02000f;
        public static final int pardinyes=0x7f020010;
        public static final int princep=0x7f020011;
        public static final int raimat=0x7f020012;
        public static final int rambla=0x7f020013;
        public static final int seca=0x7f020014;
        public static final int torres=0x7f020015;
        public static final int universitat=0x7f020016;
        public static final int xalets=0x7f020017;
    }
    public static final class id {
        public static final int action_settings=0x7f080007;
        public static final int edit_message=0x7f080003;
        public static final int hombre=0x7f080005;
        public static final int icono=0x7f080000;
        public static final int mujer=0x7f080006;
        public static final int subtitulo=0x7f080002;
        public static final int texto=0x7f080004;
        public static final int titulo=0x7f080001;
    }
    public static final class layout {
        public static final int activity_detalle=0x7f030000;
        public static final int activity_main=0x7f030001;
        public static final int activity_muestra_zonas=0x7f030002;
    }
    public static final class menu {
        public static final int main=0x7f070000;
        public static final int muestra_zonas=0x7f070001;
    }
    public static final class string {
        public static final int action_settings=0x7f050001;
        public static final int app_name=0x7f050000;
        public static final int button_send=0x7f050003;
        public static final int edit_message=0x7f050002;
        public static final int hello_world=0x7f050009;
        public static final int hombre=0x7f050005;
        public static final int indica=0x7f050007;
        public static final int mujer=0x7f050006;
        public static final int title_activity_display_message=0x7f050004;
        public static final int title_activity_muestra_zonas=0x7f050008;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
