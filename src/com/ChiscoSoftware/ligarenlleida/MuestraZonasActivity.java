package com.ChiscoSoftware.ligarenlleida;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.os.Build;
import android.app.AlertDialog;
import android.view.View;
import android.content.Intent;
import android.widget.*;
import android.app.Dialog;

public class MuestraZonasActivity extends ListActivity {
	
	Vector<String> myVector=new Vector<String>();
	Vector<String> myVector2=new Vector<String>();
	Vector<String> myVector3=new Vector<String>();
	int myNum = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_muestra_zonas);
		setupActionBar();
		
		// Get the message from the intent
	    Intent intent = getIntent();
	    String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		
	    myNum = Integer.parseInt(message.toString());
	    
	    construirVector();
	    
	    /*setListAdapter(new ArrayAdapter<String>(this,
                R.layout.activity_detalle, R.id.titulo,
                myVector));*/
	    setListAdapter(new MiAdaptador(this,
	    		myVector,myVector2));
	    
	}
	
	public void construirVector(){
		
		myVector.add("Pardinyes");
		myVector2.add("1512 posibles ligues");
		myVector.add("Princep de Viana");
		myVector2.add("939 posibles ligues");
		myVector.add("Balafia"); 
		myVector2.add("694 posibles ligues");
		myVector.add("Universitat"); 
		myVector2.add("666 posibles ligues");
		myVector.add("Instituts");
		myVector2.add("572 posibles ligues");
		myVector.add("Seca de Sant Pere"); 
		myVector2.add("564 posibles ligues");
		myVector.add("Cappont Pol�gono");
		myVector2.add("554 posibles ligues");
		myVector.add("Rambla");
		myVector2.add("421 posibles ligues");
		myVector.add("Camp d'esports"); 
		myVector2.add("409 posibles ligues");
		myVector.add("La Bordeta"); 
		myVector2.add("303 posibles ligues");
		myVector.add("Cappont"); 
		myVector2.add("229 posibles ligues");
		myVector.add("Mariola"); 
		myVector2.add("223 posibles ligues");
		myVector.add("Centre Hist�ric"); 
		myVector2.add("160 posibles ligues");
		myVector.add("Xalets - Humbert Torres"); 
		myVector2.add("82 posibles ligues");
		myVector.add("Torres de sanui");
		myVector2.add("70 posibles ligues");
		myVector.add("Joc de la bola");
		myVector2.add("62 posibles ligues");
		myVector.add("Basses d'alpicat"); 
		myVector2.add("56 posibles ligues");
		myVector.add("Raimat - Suchs"); 
		myVector2.add("32 posibles ligues");
		myVector.add("Butsenit"); 
		myVector2.add("28 posibles ligues");
		myVector.add("Magraners");
		myVector2.add("16 posibles ligues");
		myVector.add("Llivia"); 
		myVector2.add("15 posibles ligues");
		if(myNum>=0 && myNum<=5){
			myVector.add("Balafia"); //694
			myVector2.add("694 posibles ligues");
			myVector.add("Pardinyes"); //512
			myVector2.add("512 posibles ligues");
			myVector.add("La Bordeta"); //503
			myVector2.add("503 posibles ligues");
			myVector.add("Cappont"); //429
			myVector2.add("429 posibles ligues");
			myVector.add("Mariola"); //423
			myVector2.add("423 posibles ligues");
			myVector.add("Centre Hist�ric"); //360
			myVector2.add("360 posibles ligues");
			myVector.add("Princep de Viana"); //339
			myVector2.add("339 posibles ligues");
			myVector.add("Universitat"); //266
			myVector2.add("266 posibles ligues");
			myVector.add("Instituts"); //172
			myVector2.add("172 posibles ligues");
			myVector.add("Seca de Sant Pere"); //164
			myVector2.add("164 posibles ligues");
			myVector.add("Cappont Pol�gono"); //154
			myVector2.add("154 posibles ligues");
			myVector.add("Rambla"); //121
			myVector2.add("121 posibles ligues");
			myVector.add("Camp d'esports"); //109
			myVector2.add("109 posibles ligues");
			myVector.add("Xalets - Humbert Torres"); //102
			myVector2.add("102 posibles ligues");
			myVector.add("Torres de sanui"); //90
			myVector2.add("90 posibles ligues");
			myVector.add("Joc de la bola"); //82
			myVector2.add("82 posibles ligues");
			myVector.add("Magraners"); //66
			myVector2.add("66 posibles ligues");
			myVector.add("Llivia"); //55
			myVector2.add("55 posibles ligues");
			myVector.add("Basses d'alpicat"); //46
			myVector2.add("46 posibles ligues");
			myVector.add("Raimat - Suchs"); //32
			myVector2.add("32 posibles ligues");
			myVector.add("Butsenit"); //28
			myVector2.add("28 posibles ligues");
		}
		if(myNum>5 && myNum<=10){
			
		}
		if(myNum>10 && myNum<=15){
			
		}
		if(myNum>15 && myNum<=20){
			
		}
		if(myNum>20 && myNum<=25){
			
		}
		if(myNum>25 && myNum<=30){
			
		}
		if(myNum>30 && myNum<=35){
			
		}
		if(myNum>35 && myNum<=40){
			
		}
		if(myNum>40 && myNum<=45){
			
		}
		if(myNum>45 && myNum<=50){
					
		}
		if(myNum>50 && myNum<=55){
			
		}
		if(myNum>55 && myNum<=60){
			
		}
		if(myNum>60 && myNum<=65){
			
		}
		if(myNum>65 && myNum<=70){
			
		}
		if(myNum>70 && myNum<=75){
			
		}
		if(myNum>75 && myNum<=80){
			
		}
		if(myNum>80){
			
		}
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.muestra_zonas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
