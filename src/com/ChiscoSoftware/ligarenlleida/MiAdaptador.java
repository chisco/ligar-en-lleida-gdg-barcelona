package com.ChiscoSoftware.ligarenlleida;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.os.Build;
import android.app.AlertDialog;
import android.view.View;
import android.content.Intent;
import android.widget.*;
import android.app.Dialog;

public class MiAdaptador extends BaseAdapter {
    
	private final Activity actividad;
    private final Vector<String> lista;
    private final Vector<String> lista2;

    public MiAdaptador(Activity actividad, Vector<String> lista, Vector<String> lista2) {
          super();
          this.actividad = actividad;
          this.lista = lista;
          this.lista2 = lista2;
    }

    public View getView(int position, View convertView, 
                                     ViewGroup parent) {
          LayoutInflater inflater = actividad.getLayoutInflater();
          View view = inflater.inflate(R.layout.activity_detalle, null,true);
          
          TextView textView =(TextView)view.findViewById(R.id.titulo);
          textView.setText(lista.elementAt(position));
          
          TextView textView2 =(TextView)view.findViewById(R.id.subtitulo);
          textView2.setText(lista2.elementAt(position));
          
          ImageView imageView=(ImageView)view.findViewById(R.id.icono);
          
          if(lista.elementAt(position)=="Balafia"){
        	  imageView.setImageResource(R.drawable.balafia);
          }
          if(lista.elementAt(position)=="Pardinyes"){
        	  imageView.setImageResource(R.drawable.pardinyes);
          }
          if(lista.elementAt(position)=="La Bordeta"){
        	  imageView.setImageResource(R.drawable.bordetta);
          }
          if(lista.elementAt(position)=="Cappont"){
        	  imageView.setImageResource(R.drawable.cappont);
          }
          if(lista.elementAt(position)=="Mariola"){
        	  imageView.setImageResource(R.drawable.mariola);
          }
          if(lista.elementAt(position)=="Centre Hist�ric"){
        	  imageView.setImageResource(R.drawable.centre);
          }
          if(lista.elementAt(position)=="Princep de Viana"){
        	  imageView.setImageResource(R.drawable.princep);
          }
          if(lista.elementAt(position)=="Universitat"){
        	  imageView.setImageResource(R.drawable.universitat);
          }
          if(lista.elementAt(position)=="Instituts"){
        	  imageView.setImageResource(R.drawable.instituts);
          }
          if(lista.elementAt(position)=="Seca de Sant Pere"){
        	  imageView.setImageResource(R.drawable.seca);
          }
          if(lista.elementAt(position)=="Cappont Pol�gono"){
        	  imageView.setImageResource(R.drawable.cappont_poli);
          }
          if(lista.elementAt(position)=="Rambla"){
        	  imageView.setImageResource(R.drawable.rambla);
          }
          if(lista.elementAt(position)=="Camp d'esports"){
        	  imageView.setImageResource(R.drawable.camp);
          }
          if(lista.elementAt(position)=="Xalets - Humbert Torres"){
        	  imageView.setImageResource(R.drawable.xalets);
          }
          if(lista.elementAt(position)=="Torres de sanui"){
        	  imageView.setImageResource(R.drawable.torres);
          }
          if(lista.elementAt(position)=="Joc de la bola"){
        	  imageView.setImageResource(R.drawable.joc);
          }
          if(lista.elementAt(position)=="Magraners"){
        	  imageView.setImageResource(R.drawable.magraners);
          }
          if(lista.elementAt(position)=="Llivia"){
        	  imageView.setImageResource(R.drawable.llivia);
          }
          if(lista.elementAt(position)=="Basses d'alpicat"){
        	  imageView.setImageResource(R.drawable.basses);
          }
          if(lista.elementAt(position)=="Raimat - Suchs"){
        	  imageView.setImageResource(R.drawable.raimat);
          }
          if(lista.elementAt(position)=="Butsenit"){
        	  imageView.setImageResource(R.drawable.butsenit);
          }
          return view;
    }

    public int getCount() {
          return lista.size();
    }

    public Object getItem(int arg0) {
          return lista.elementAt(arg0);
    }

    public long getItemId(int position) {
          return position;
    }
}