package com.ChiscoSoftware.ligarenlleida;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Menu;
import android.view.View;
import android.content.Intent;
import android.widget.*;
import android.app.Dialog;

public class MainActivity extends Activity {
	
	private Boolean genero;
	public final static String EXTRA_MESSAGE = "com.ChiscoSoftware.ligarenlleida.MESSAGE";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        genero = true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.hombre:
                if (checked)
                	genero = true;
                break;
            case R.id.mujer:
                if (checked)
                	genero = false;
                break;
        }
    }
    
    public void sendMessage(View view) {
    	/*new AlertDialog.Builder(this)
        .setTitle("Confirmar")
        .setMessage("�Buscar zonas con estos datos?")
        .setPositiveButton("Yes", null)
        .setNegativeButton("No", null)
         .show();*/
    	Intent intent = new Intent(this, MuestraZonasActivity.class);
    	EditText editText = (EditText) findViewById(R.id.edit_message);
    	String message = editText.getText().toString();
    	if(message.equals("")){
    		new AlertDialog.Builder(this)
            .setTitle("Error")
            .setMessage("Por favor, introduce una edad")
            .setPositiveButton("Ok", null)
             .show();
    	}else{
    		intent.putExtra(EXTRA_MESSAGE, message);
    		startActivity(intent);
    	}
    }
    
}
